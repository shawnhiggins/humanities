				<?php // For posts



				if (is_single() || is_archive() || is_search()) { ?>
				<div class="col side" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<a class="btn" href="/news/">View All</a>
					<?php else : endif; ?>
					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				
				<?php // For pages
				if (is_page() || is_single() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Profiles subpage
								if (is_tree(1305, 108) || get_field('menu_select') == "profile" || is_single('person', 'career', 'careers' ) ) {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Profile', 'bonestheme' ),
										'menu_class' => 'profilee-nav',
										'theme_location' => 'profile-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Student</h3> <ul>%3$s</ul>'
									));
								}
								
								// If an Commencement subpage
								if (is_tree(860, 139) || get_field('menu_select') == "events") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Events', 'bonestheme' ),
									   	'menu_class' => 'events-nav',
									   	'theme_location' => 'events-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Events</h3> <ul>%3$s</ul>'
									));
								}
                    // If an ABOUT subpage
								if (is_tree(101) || get_field('menu_select') == "about-us") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'About Us', 'bonestheme' ),
									   	'menu_class' => 'about-nav',
									   	'theme_location' => 'about-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>About Us</h3> <ul>%3$s</ul>'
									));
								}
								
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_tree(9999) || is_search() || is_404() || is_page('contact') || is_page('about') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
						<?php if ( is_page( 'give' )  ) : ?>
							<?php dynamic_sidebar( 'give-sidebar' ); ?>
						<?php else : ?>
					
						<?php endif; ?>
					</div>
				</div>
					
				<?php } ?>