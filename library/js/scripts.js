/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y }
}

// setting the viewport width
var viewport = updateViewportDimensions();

/*
 * Put all your regular jQuery in here.
*/

jQuery("document").ready(function($) {

/********************************
 * Code for fixed scrolling menus
*/
// Enter the class of the block you want to stick.
// http://leafo.net/sticky-kit/

	$(".nav-container").stick_in_parent()

/********************************
 * Smooth scrolling effect when using anchor links
*/
	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
				}
			}
		});
	});

/********************************
 * FAQ Toggle
*/
	$(document).ready(function() {
		$("dd.answer").hide();
		$("dt.question").click(function(){
			$(this).toggleClass("active").next().slideToggle("normal");
			return false;
		});
	});

/********************************
 * Profile Page Filter
*/

	$(document).ready(function() {
	// init Isotope
		var $container = $('.profile-list').isotope({
			itemSelector: '.person-item'
		});
		/*
		// Uncommont to change default filter from view all
		$container.isotope({
			filter: '.football'
		});
		*/
		// store filter for each group
		var filters = {};

		$('.filter').on( 'click', '.option', function() {
			var $this = $(this);
			// get group key
			var $buttonGroup = $this.parents('.button-group');
			var filterGroup = $buttonGroup.attr('data-filter-group');
			// set filter for group
			filters[ filterGroup ] = $this.attr('data-filter');
			// combine filters
			var filterValue = concatValues( filters );
			// set filter for Isotope
			$container.isotope({ filter: filterValue });
	  });

	  // change is-checked class on buttons
		$('.button-group').each( function( i, buttonGroup ) {
			var $buttonGroup = $( buttonGroup );
			$buttonGroup.on( 'click', 'button', function() {
				$buttonGroup.find('.is-checked').removeClass('is-checked');
				$( this ).addClass('is-checked');
			});
		});
	});

	// flatten object by concatting values
	function concatValues( obj ) {
		var value = '';
		for ( var prop in obj ) {
			value += obj[ prop ];
		}
		return value;
	}

/********************************
 * Accessible Dropdown Menus
*/
	(function ($) {
		"use strict";
		$(document).ready(function () {
			// initialize the megamenu
			$('.megamenu').accessibleMegaMenu();
			// hack so that the megamenu doesn't show flash of css animation after the page loads.
			setTimeout(function () {
				$('body').removeClass('init');
			}, 500);
		});
	}
	(jQuery));
	
	// Finds the first nav menu on the page
	$("nav:first").accessibleMegaMenu({
		/* prefix for generated unique id attributes, which are required 
		to indicate aria-owns, aria-controls and aria-labelledby */
		uuidPrefix: "accessible-menu",
		
		// css class used to define the megamenu styling
		menuClass: "main-nav",
		
		// css class for a top-level navigation item in the megamenu
		topNavItemClass: "parent-item",
		
		// css class for a megamenu panel
		panelClass: "sub-menu",
		
		// css class for a group of items within a megamenu panel
		panelGroupClass: "sub-menu",
		
		// css class for the hover state
		hoverClass: "hover",
		
		// css class for the focus state
		focusClass: "focus",
		
		// css class for the open state
		openClass: "open"
	});
	
/********************************
 * End
*/
});