<?php get_header(); ?>

			<div class="content main" id="main-content">
				<header>
                    
                    <?php 
                        $cat = get_term_by('name', single_cat_title('', false), 'happenings_cat');
                        $current_cat_name = $cat->name;
                        $current_cat_slug = $cat->slug;
                        //echo $current_cat_slug;
                    ?>
					<h1><?php echo $current_cat_name; ?></h1>  
                        
                        <?php echo category_description( get_category_by_slug('category-slug')->term_id ); ?>                  	
				</header>
                                
				
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">						
						<section class="entry-content cf">
                            <?php if ( has_post_thumbnail() ) { 
                                    $trim_number = 30;
                            ?>
                            <figure class="blog-thumb">                                
				                <?php the_post_thumbnail( 'dean-thumb' ); ?>                            
                            </figure>                         
                            <div class="details">                             
                            <?php }else { 
                                    $trim_number = 100;
                            ?>
                            <div class="details-wide">
                            <?php } ?>
                                <span class="cat-title">
                                    <?php echo get_the_term_list( $post->ID, 'category', '', ' | ' , ''); ?>                                
                                </span>
                                <h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                                    <dl>
                                        <?php if(get_field('first_name')) { ?>
                                            <dt class="name">
                                                <h3><?php the_field('first_name'); ?> <?php the_field('last_name'); ?></h3>
                                            </dt>
                                        <?php } ?>
                                        <?php if(get_field('date_of_event')) { ?>
                                        <span class="date">
                                            <strong>Date: </strong><?php the_field('date_of_event'); ?> | 
                                        </span>
                                        <?php } ?>
                                        <?php if(get_field('time_of_event')) { ?>
                                        <span class="time">
                                            <strong>Time: </strong><?php the_field('time_of_event'); ?> | 
                                        </span>
                                        <?php } ?>
                                        <?php if(get_field('location')) { ?>
                                        <span class="location">
                                            <strong>Location: </strong><?php the_field('location'); ?>
                                        </span>
                                        <?php } ?>
                                
                                    </dl>
									
                                  <!--// span class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></span //-->
                                    <p>
                                        <?php
                                            $content = get_the_content();
                                            $trimmed_content = wp_trim_words( $content, $trim_number, '...' );
                                            echo $trimmed_content;
                                        ?>
                                    </p>
                                <a href="<?php the_permalink() ?>" class="btn">Read More</a>
                            </div>
						</section>
					</article>

					<?php endwhile; ?>
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>
			</div>					
					<div class="content col side">
                        <nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php								
                                wp_nav_menu(array(
                                    'container' => false,
                                    'menu' => __( 'Events', 'bonestheme' ),
                                    'menu_class' => 'events-nav',
                                    'theme_location' => 'events-nav',
                                    'before' => '',
                                    'after' => '',
                                    'depth' => 2,
                                    'items_wrap' => '<h3>Events</h3> <ul>%3$s</ul>'
                                ));

							?>
						</nav>
					</div>            
				<!--// ?php get_sidebar(); ? //-->
</div>

<?php get_footer(); ?>