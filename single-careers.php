<?php get_header(); ?>
			<div class="content main">
                        <h1 id="bio"><?php the_title(); ?></h1>
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">                 
							
							<?php the_post_thumbnail( 'content-width' ); ?>
						<div class="details">
                            <dl>
                                <?php if(get_field('first_name')) { ?>
                                    <dt class="name">
                                        <h3><?php the_field('first_name'); ?> <?php the_field('last_name'); ?></h3>
                                    </dt>
                                <?php } ?>
                                <?php if(get_field('date_of_event')) { ?>
                                <span class="date">
                                    <strong>Date: </strong><?php the_field('date_of_event'); ?> | 
                                </span>
                                <?php } ?>
                                <?php if(get_field('time_of_event')) { ?>
                                <span class="time">
                                    <strong>Time: </strong><?php the_field('time_of_event'); ?> |  
                                </span>
                                <?php } ?>
                                <?php if(get_field('location')) { ?>
                                <span class="location">
                                    <strong>Location: </strong><?php the_field('location'); ?>
                                </span>
                                <?php } ?>

                            </dl>									
						</div>
						<section class="bio">
							<?php the_content(); ?>
						</section>
						<?php if(get_field('education')) { ?>
						<section id="education">
							<h2>Education</h2>
							<?php the_field('education'); ?>
						</section>
						<?php } ?>
					</article>
					<?php endwhile; ?>
					<?php else : endif; ?>
				</div>
				<div class="col">					
					<div class="content col side">
                        <nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Profiles subpage								
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Profile', 'bonestheme' ),
										'menu_class' => 'profilee-nav',
										'theme_location' => 'profile-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Students</h3> <ul>%3$s</ul>'
									));
							?>
						</nav>
					</div>
				</div>                
				<!--// ?php get_sidebar(); ? //-->
			</div>
<?php get_footer(); ?>