<?php get_header(); ?>
			<div class="content main">
                        <span class="cat-title">                                
                            <?php echo get_the_term_list( $post->ID, 'profile_cat', '', ',' , ''); ?>
                        </span>
                        <h1 id="bio"><?php the_title(); ?></h1>
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">
                        <figure>                        
                            <?php if(get_field('photo')) {
                                $image = get_field('photo');
                                if( !empty($image) ): 
                                // vars
                                $url = $image['url'];
                                $title = $image['title'];
                                // thumbnail
                                $size = 'blog-large';
                                $thumb = $image['sizes'][ $size ];
                                $width = $image['sizes'][ $size . '-width' ];
                                $height = $image['sizes'][ $size . '-height' ];
                            endif; ?>
                            <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                            <?php } else { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-default-img.png" alt="Silhouette" class="photo default-img <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                            <?php } ?>	
                        </figure>                        
						
						<div class="details">
							<?php if(get_field('email_address')) { ?>
								<span><strong>E-mail: </strong><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a></span>
							<?php } ?>
						</div>
						<section class="bio">
							<?php the_content(); ?>
						</section>
						<?php if(get_field('education')) { ?>
						<section id="education">
							<h2>Education</h2>
							<?php the_field('education'); ?>
						</section>
						<?php } ?>
					</article>
					<?php endwhile; ?>
					<?php else : endif; ?>
				</div>
				<div class="col">					
					<div class="content col side">
                        <nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Profiles subpage								
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Profile', 'bonestheme' ),
										'menu_class' => 'profilee-nav',
										'theme_location' => 'profile-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Student</h3> <ul>%3$s</ul>'
									));
							?>
						</nav>
					</div>
				</div>                
				<!--// ?php get_sidebar(); ? //-->
			</div>
<?php get_footer(); ?>