<?php
/*
 Template Name: Home Page
*/
?>
<?php get_header(); ?>
			<div id="main-content" role="main">

				<?php if(get_field('hero_type', 'option') == "single") {
					$rows = get_field('hero_image' ); // get all the rows
					$rand_row = $rows[ array_rand( $rows ) ]; // get a random row
					$silder_image = $rand_row['image' ]; // get the sub field value 
					$slider_description = $rand_row['description' ]; // get the sub field value 
					$slider_link = $rand_row['link' ]; // get the sub field value 			
					if( !empty($silder_image) ): 
						// vars
						$url = $silder_image['url'];
						$title = $silder_image['title'];
						// thumbnail
						$size = 'home-hero';
						$slide = $silder_image['sizes'][ $size ];
						$width = $silder_image['sizes'][ $size . '-width' ];
						$height = $silder_image['sizes'][ $size . '-height' ];
					endif;
				?>
				<?php if( $silder_image ): ?>
				<?php if( $slider_link ): ?>
				<a href="<?php echo $home_button_link; ?>" class="hero-link">
				<?php endif; ?>
					<div id="hero" style="background-image:url('<?php echo $slide; ?>');">
						<div class="content">
						<?php if( $slider_description ): ?>
							<div class="hero-description">
								<?php echo $slider_description; ?>
							</div>
						<?php endif; ?>
						</div>
					</div>
				<?php if( $slider_link ): ?>
				</a>
				<?php endif; ?>
				<?php endif; ?>
				<?php } ?>
				
				<?php // If set to slider, we use Slider custom post type
					if(get_field('hero_type', 'option') == "slider") { 
				?>
					<script type="text/javascript">
						jQuery("document").ready(function($) {
							$(document).ready(function(){
							  $('#bxslider').bxSlider({
							  	autoHover: true,
							  	auto: false,
							  });
							});
						});
					</script>
					<div id="slider">
						<ul id="bxslider">
							<?php if( have_rows('hero_image') ): ?>
							<?php while( have_rows('hero_image') ): the_row(); ?>
							<?php
								$slider_title = get_sub_field('title');
								$slider_description = get_sub_field('description');
								$slider_link = get_sub_field('link');
								$silder_image = get_sub_field('image');
								if( !empty($silder_image) ): 
									// vars
									$url = $silder_image['url'];
									$title = $silder_image['title'];
									// thumbnail
									$size = 'home-hero';
									$slide = $silder_image['sizes'][ $size ];
									$width = $silder_image['sizes'][ $size . '-width' ];
									$height = $silder_image['sizes'][ $size . '-height' ];
								endif;
							?>		
							<?php if( $slider_link ): ?>
							<a href="<?php echo $slider_link; ?>" class="hero-link">
							<?php endif; ?>
							<li style="background-image: url('<?php echo $slide; ?>');">
								<div class="content">
									<div class="slider-content">
										<?php if( $slider_title ): ?>
										<h2><?php echo $slider_title; ?></h2>
										<?php endif; ?>
										<?php if( $slider_description ): ?>
										<p><?php echo $slider_description; ?></p>
										<?php endif; ?>
									</div>
								</div>
							</li>
							<?php if( $slider_link ): ?>
							</a>
							<?php endif; ?>
							<?php endwhile; ?>
							<?php endif; ?>
						</ul>
					</div>
				<?php } ?>
				<div class="content">
                    <div class="col profile-col">
                        <h3>Profiles</h3>
                        <?php get_template_part( 'lastest_profiles' ); ?>
					</div>
					<div class="col news-col">
						<h3>Latest News</h3>
						<?php query_posts( array ( 'showposts' => 4	) ); ?>
						<ul>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							
							<a href="<?php the_permalink() ?>">
								<li>
									<?php if ( has_post_thumbnail() ) {
										$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article-thumb' );
										$url = $thumb['0']; ?>
										<img src="<?=$url?>" alt="<?php the_title(); ?>" />
									<?php } else { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-default-img.png" alt="Silhouette" class="photo default-img <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                            <?php } ?>	
									<div class="news-item">
										<h4><?php the_title(); ?></h4>
										<p><?php
											$content = get_the_content();
											$trimmed_content = wp_trim_words( $content, 13, '...' );
											echo $trimmed_content;
										?></p>
									</div>
								</li>
							</a>							
							<?php endwhile; ?>
						</ul>
						<?php endif; ?>
						<a class="btn" href="/news/">View All</a>
					</div>					
					<div class="col events-col">
						<ul>
							<?php if ( is_active_sidebar( 'events-sidebar' ) ) : ?>
								<?php dynamic_sidebar( 'events-sidebar' ); ?>				
							<?php else : endif; ?>
						</ul>
					</div>
				</div>
			</div>
<?php get_footer(); ?>