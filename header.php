<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js ie lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js ie lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js ie lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js ie"><!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php wp_title(''); ?></title>
		<!--<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>-->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#cc3333">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<?php wp_head(); ?>
		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>
	</head>
	<body <?php body_class(); ?>>
		<a href="#main-content" class="hidden skip">Skip to main content</a>
		<div id="container">
            <div class="top_wrp">
			<header role="banner" class="top">
				<div class="content">
					<a href="<?php echo home_url(); ?>" rel="nofollow">
						<!--// img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo.png" alt="UCLA" class="university-logo" /><span class="hidden">UCLA</span //-->
						<?php // H1 if homepage, H2 otherwise.
							if ( is_front_page() ) { ?>
						<h1><img src="<?php echo get_template_directory_uri(); ?>/library/images/dept-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span></h1>
						<?php } else { ?>
						<h2><img src="<?php echo get_template_directory_uri(); ?>/library/images/dept-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span></h2>
						<?php }?>
					</a>
					<?php if(get_field('enable_donation', 'option') == "enable") { ?>
					<div class="give-back">
						<?php if(get_field('link_type', 'option') == "internal") { ?>
						<a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
						<?php }?>
						<?php if(get_field('link_type', 'option') == "external") { ?>
						<a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
						<?php }?>
						<?php the_field('button_text', 'option'); ?></a>
						<?php if(get_field('supporting_text', 'option')) { ?>
						<span><?php the_field('supporting_text', 'option'); ?></span>
						<?php }?>
					</div>
					<?php }?>
                        
                        
				<!--// ?php get_search_form(); ? //-->                     
                        
				<nav role="navigation" aria-labelledby="main navigation">
					<?php wp_nav_menu(array(
						'container' => false,
						'menu' => __( 'Main Menu', 'bonestheme' ),
						'menu_class' => 'main-nav',
						'theme_location' => 'main-nav',
						'before' => '',
						'after' => '',
						'depth' => 2,
					)); ?>
				</nav>
                        
				</div>
			</header>
                </div>
			<?php 
				// Don't do any of the below if homepage
				if ( is_front_page() ) { }
				
				// Breadcrumb everywhere else
				elseif ( is_single() || is_category( $category ) || is_archive() ) { 
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
					}
				}
				else {
					// Only show hero image on a page or post
					if ( has_post_thumbnail() && is_single() || has_post_thumbnail() && is_page() ) {
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
						$url = $thumb['0']; ?>
			<div id="hero" style="background-image:url('<?=$url?>');">
				&nbsp;
			</div>
			<?php }
					// And show breadcrumb
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
					} 
				} 
			?>