<?php
/**
 * Display 3 random testimonials
 */

$args = array(
	'post_type' => 'profile',
	'posts_per_page' => 1,
	'orderby' => 'rand'

);

$profiles = new WP_Query( $args );

?>

<aside id="profiles-display" class="profile-list clear">
					<ul>
						<?php while ( $profiles->have_posts() ) : $profiles->the_post(); ?>
						<li class="person-item">
                            <?php if(get_field('photo')) {
							$image = get_field('photo');
							if( !empty($image) ): 
								// vars
								$url = $image['url'];
								$title = $image['title'];
								// thumbnail
								$size = 'blog-thumb';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];
							endif; ?>
							<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
							<?php } else { ?>
							<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-default-img.png" alt="A photo of <?php the_title(); ?>" class="photo default-img <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
							<?php } ?>
							<dl>
                                <span class="cat-title">                                    
                                    <?php echo get_the_term_list( $post->ID, 'profile_cat', '', ',' , ''); ?>                                
                                </span>
                                <dt class="name"><?php the_title(); ?></dt>
                                <dd class="description">
                                <p>
                                    <?php
                                    $content = get_the_content();
                                    $trimmed_content = wp_trim_words( $content, 30, '...' );
                                    echo $trimmed_content; 
                                    ?>    
                                    <a href="<?php the_permalink() ?>">Read More</a>
                                </p>
                                </dd>
				            </dl>
                                
							</a>
                            <a class="btn" href="/profiles">View All</a>
						</li>
					<?php endwhile; ?>					
					</ul>
			</aside>

<!--//


echo '<aside id="testimonials" class="clear">';
while ( $testimonials->have_posts() ) : $testimonials->the_post();
    echo '<div class="testimonial">';
    echo '<figure class="testimonial-thumb">';
    the_post_thumbnail('medium');
    echo '</figure>';
    echo '<h1 class="entry-title">' . get_the_title() . '</h1>';
    echo '<div class="entry-content">';
    the_content();
    echo '</div>';
    echo '</div>';
endwhile;
echo '</aside>';

//-->


<?php wp_reset_query(); ?>