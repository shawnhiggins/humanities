<?php
/**
 * Month View Template
 * The wrapper template for month view.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

<?php do_action( 'tribe_events_before_template' ) ?>

<!-- Tribe Bar -->
<?php tribe_get_template_part( 'modules/bar' ); ?>
<!--// ?php
		echo tribe_get_event_categories(
			get_the_id(), array(
				'before'       => '<li>',
				'sep'          => '</li><li> ',
				'after'        => '</li>',
				'label'        => 'Departments', // An appropriate plural/singular label will be provided
				'label_before' => '<dt>',
				'label_after'  => '</dt>',
				'wrap_before'  => '<ul class="tribe-events-event-categories">',
				'wrap_after'   => '</ul>',
			)
		);
		? //-->

<div>
<strong>Departments</strong>
<select name="event-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'> 
 <option value=""><?php echo esc_attr(__('Select Event')); ?></option> 
 <?php

$args = array(
	'type'                     => 'post',
	'child_of'                 => 0,
	'parent'                   => '',
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'hide_empty'               => 1,
	'hierarchical'             => 1,
	'exclude'                  => '',
	'include'                  => '',
	'number'                   => '',
	'taxonomy'                 => 'tribe_events_cat',
	'pad_counts'               => false 

); 
  $categories = get_categories($args); 
//echo = $categories;
  foreach ($categories as $category) {
  	$option = '<option value="/events/category/'.$category->category_nicename.'">';
	$option .= $category->cat_name;
	$option .= ' ('.$category->category_count.')';
	$option .= '</option>';
	echo $option;
  }
 ?>
</select>

<br /><br /></div>



<!-- Main Events Content -->
<?php tribe_get_template_part( 'month/content' ); ?>

<?php do_action( 'tribe_events_after_template' ) ?>
