<?php
/**
 * Template Name: Academics Category Index Page
 */
    get_header(); ?>
			<div class="content main" id="main-content">
				<header>
					<h1><?php the_title(); ?></h1>
                    			<?php the_content(); ?>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '<p>' . $category_description . '</p>' );
					?>
					<?php if ( has_nav_menu( 'faculty-filter' ) ) {?> 
					<div class="filter">
					<?php // To make another filter, duplicate the div below ?>
						<div class="options button-group" data-filter-group="field">
							<h3>Field of Study</h3>
							<ul>
								<button data-filter="" class="option all is-checked">View All</button>
								<?php wp_nav_menu(array(
									'container' => false,
									'menu' => __( 'Faculty Filter', 'bonestheme' ),
									'menu_class' => 'faculty-filter',
									'theme_location' => 'faculty-filter',
									'before' => '',
									'after' => '',
									'depth' => 1,
									'items_wrap' => '%3$s',
									'walker' => new Filter_Walker
								)); ?>
							</ul>
						</div>
					</div>
					<?php if( have_rows('filters', 'option') ): ?>
					<script type="text/javascript">
						jQuery("document").ready(function($) {
							$('.all').click(function() {
								$('.filter-title').html('All');
							});
							<?php while( have_rows('filters', 'option') ): the_row();
							// vars
								$class = get_sub_field('class');
								$category = get_sub_field('category');
							?>
							$('.filter .<?php echo $class ?>').click(function() {
								$('.filter-title').html('<?php echo $category->name; ?>');
							});						
							<?php endwhile; ?>
						});
					</script>
					<?php endif; ?>					
					<h2 class="filter-title">All</h2>
					<?php } ?> 
				</header>
				<div class="academics-list">
					<ul <?php post_class('cf'); ?>>
                        <?php $core_loop = new WP_Query( array( 'academics_cat' => array('departments', 'centers', 'research centers', 'languages'), 'post_type' => 'academics', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC')); ?>
                        
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						<li class="academics-item">
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'blog-thumb';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette 
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-default-img.png" alt="A photo of <?php the_title(); ?>" class="photo default-img <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="description">
                                        
                                    <?php if(get_the_term_list( $post->ID, 'academics_cat')){ ?>
                                        <?php echo get_the_term_list( $post->ID, 'academics_cat', '', ' | ' , ''); ?>                                       
                                            <br />
                                    <?php } ?>
                                    <?php if(get_field('email_address')) { ?>
                                        <span class="email">
                                            <strong>Email: </strong><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a> | 
                                        </span>
                                        <?php } ?>
                                        <?php if(get_field('phone_number')) { ?>
                                        <span class="phone">
                                            <strong>Phone: </strong><?php the_field('phone_number'); ?> | 
                                        </span>
                                        <?php } ?>
									<?php if(get_field('office_hours')) { ?>
									<span class="office_hours">
										<strong>Office Hours: </strong><?php the_field('office_hours'); ?>
									</span>
									<?php } ?>
                                        <p>
											<?php
											$content = get_the_content();
											$trimmed_content = wp_trim_words( $content, 90, '...' );
											echo $trimmed_content;
											?>
										</p></dd>
                                    <?php if(get_the_term_list( $post->ID, 'languages_cat')){ ?>
                                        <dt>Languages offered:</dt>
                                        <dd class="language">                                        
                                        <!--// ?php echo get_the_term_list( $post->ID, 'languages_cat', '<span class="btn">', '</span> <span class="btn">' , '</span>'); ? //-->
                                            <?php
                                                                                             
                                                $product_terms = wp_get_object_terms($post->ID, 'languages_cat');
                                                if ( ! empty( $product_terms ) ) {
                                                    if ( ! is_wp_error( $product_terms ) ) {
                                                        echo '<div>';
                                                            foreach( $product_terms as $term ) {
                                                                echo '<span class="btn">' . esc_html( $term->name ) . '</span>'; 
                                                            }
                                                        echo '</div>';
                                                    }
                                                }
                                            
                                            ?>
                                        </dd>                                    
                                    <?php } ?>
                                    <dd>
                                
                            <!--// a class="btn" href="<?php the_permalink() ?>">Read More</a //-->
                            <?php if(get_field('groups_website')) { ?>
									<a class="btn" target="_blank" href="<?php the_field('groups_website'); ?>">Website</a>
									<?php } ?>
                                    
                                    </dd>
								</dl>
						</li>
					<?php endwhile; ?>				
					</ul>
				</div>
			</div>
<?php get_footer(); ?>