<?php
// Academics Post Type Settings

// add custom categories
register_taxonomy( 'academics_cat', 
	array('academics'), /* if you change the name of register_post_type( 'academics', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Departments & Programs Categories', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Academic Category', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Departments & Programs Categories', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Academic Categories', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Academic Category', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Academic Category:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Academic Category', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Academic Category', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Academic Category', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Academic Category Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_ui' => true,
        'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'academics' )
	)
);

// add custom tags
register_taxonomy( 'languages_cat', 
	array('academics'), /* if you change the name of register_post_type( 'courses_type', then you have to change this */
	array('hierarchical' => false,    /* if this is false, it acts like tags */
		'labels' => array(
			'name' => __( 'Languages', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Language', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Languages', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Language', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Language', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Language:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Languages', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Language', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Language', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Language Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_tagcloud' => true,
		'show_ui' => true,
		
		'query_var' => true,
	)
);


// let's create the function for the custom type
function academics_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'academics', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Academics', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Departments', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Academic', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Departments', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Departments', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Departments', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Departments', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Academics', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No academics added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all members of the department', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 5, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-building', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'departments-programs', 'with_front' => true ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'revisions', 'author' )
			
		) /* end of options */
	); /* end of register post type */
}

// adding the function to the Wordpress init
add_action( 'init', 'academics_post_type');	
?>