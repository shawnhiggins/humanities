<?php
/**
 * Events List Widget Template
 * This is the template for the output of the events list widget.
 * All the items are turned on and off through the widget admin.
 * There is currently no default styling, which is needed.
 *
 * This view contains the filters required to create an effective events list widget view.
 *
 * You can recreate an ENTIRELY new events list widget view by doing a template override,
 * and placing a list-widget.php file in a tribe-events/widgets/ directory
 * within your theme directory, which will override the /views/widgets/list-widget.php.
 *
 * You can use any or all filters included in this file or create your own filters in
 * your functions.php. In order to modify or extend a single filter, please see our
 * readme on templates hooks and filters (TO-DO)
 *
 * @return string
 *
 * @package TribeEventsCalendar
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_plural = tribe_get_event_label_plural();

$posts = tribe_get_list_widget_events();

// Check if any event posts are found.
if ( $posts ) : ?>

	<ol class="hfeed vcalendar">
		<?php
		// Setup the post data for each event.
		foreach ( $posts as $post ) :
			setup_postdata( $post );
			?>
			<a href="<?php echo esc_url( tribe_get_event_link() ); ?>" rel="bookmark">
				<li class="tribe-events-list-widget-events <?php tribe_events_event_classes() ?>">
                    <?php
                                                                                             
                        $product_terms = wp_get_object_terms($post->ID, 'tribe_events_cat');
                        if ( ! empty( $product_terms ) ) {
                            if ( ! is_wp_error( $product_terms ) ) {
                                echo '<div>';
                                    foreach( $product_terms as $term ) {
                                        echo '<span class="cat-title">' . esc_html( $term->name ) . '</span>'; 
                                    }
                                echo '</div>';
                            }
                        }

                    ?>
					<?php do_action( 'tribe_events_list_widget_before_the_event_title' ); ?>
					<!-- Event Title -->
					<h4 class="entry-title summary"><?php the_title(); ?></h4>
	
					<?php do_action( 'tribe_events_list_widget_after_the_event_title' ); ?>
					<!-- Event Time -->
	
					<?php do_action( 'tribe_events_list_widget_before_the_meta' ) ?>
	
					<div class="duration">
						<?php echo tribe_events_event_schedule_details(); ?>
					</div>
	
					<?php do_action( 'tribe_events_list_widget_after_the_meta' ) ?>
				</li>
			</a>
		<?php
		endforeach;
		?>
	</ol><!-- .hfeed -->
	<a href="<?php echo esc_url( tribe_get_events_link() ); ?>" rel="bookmark" class="btn"><?php printf( __( 'View All', 'tribe-events-calendar' ), $events_label_plural ); ?></a>
<?php
// No events were found.
else : ?>
	<p>There are no upcoming events.</p>
	<a class="btn" href="/events/list/?action=tribe_list&tribe_paged=1&tribe_event_display=past">Past Events</a>
<?php
endif;